'use strict';

const logsLocation = "/data/system/logs";
const logLevel = "silly"; // TODO: on prod use info

const fs = require("fs").promises;
const winston = require("winston");
require('winston-daily-rotate-file');

// Supported levels:
// 0. Error    <- logged to error-YYYY-MM-DD-HH.log file
// 1. Warn
// 2. Info     <- logged on prodution to journal-YYYY-MM-DD-HH.log file
// 3. Verbose  <- seems this is useless
// 4. Debug
// 5. Silly    <- logged on dev to journal-YYYY-MM-DD-HH.log file

// const stackTraceFormat = winston.format(info => {
//     if (info.meta && info.meta instanceof Error) {
//         info.message = `${info.message} ${info.meta.stack}`;
//     }
//     return info;
// });

class Log {

  constructor() { }

  async init() {
    try {
      await fs.access(fileName, fs.constants.F_OK);
    } catch (err) {
      await fs.mkdir(logsLocation, {
        recursive: true
      });
    }
    this.register(this);
    winston.loggers.get("Log").debug("Logging initialized");
  }

  register(classInstance) {

    const name = classInstance.constructor.name;

    if (name !== "Log") winston.loggers.get("Log")
      .silly(`Adding class ${name} to logger...`);

    // TODO: do not add if logger already exists

    winston.loggers.add(name, {
      level: logLevel,
      exitOnError: false,
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
            winston.format.label({
              label: name
            }),
            winston.format.timestamp(),
            winston.format.colorize(),
            winston.format.simple(),
            // stackTraceFormat,
            winston.format.printf(({
              level,
              message,
              label,
              timestamp
            }) => {
              return `${timestamp} [${label}] ${level}: ${message}`;
            })
          )
        }),
        new winston.transports.DailyRotateFile({
          format: winston.format.combine(
            winston.format.label({
              label: name
            }),
            winston.format.timestamp(),
            // stackTraceFormat,
            winston.format.json()
          ),
          filename: `${logsLocation}/journal-%DATE%.log`,
          datePattern: 'YYYY-MM-DD-HH',
          zippedArchive: true,
          maxSize: '64m',
          maxFiles: '14d',
        }),
        new winston.transports.DailyRotateFile({
          level: "error",
          filename: `${logsLocation}/errors-%DATE%.log`,
          datePattern: 'YYYY-MM-DD-HH',
          zippedArchive: true,
          maxSize: '64m',
          maxFiles: '14d',
          format: winston.format.combine(
            winston.format.label({
              label: name
            }),
            winston.format.timestamp(),
            winston.format.simple(),
            // stackTraceFormat,
            winston.format.printf(({
              message,
              label,
              timestamp
            }) => {
              return `${timestamp} ${label}: ${message}`;
            })
          )
        })
      ]
    });

    if (name !== "Log") winston.loggers.get("Log")
      .silly(`Class ${name} added`);

    let logger = winston.loggers.get(name);
    // TODO
    logger["isSilly"] = () => logger.levels[logger.level] >= logger.levels['silly'];
    logger["isDebug"] = () => logger.levels[logger.level] >= logger.levels['debug'];
    return logger;

  }

}

module.exports = Log;
