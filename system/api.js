'use strict';

const querystring = require('querystring');

let m;
let l;

class API {

  constructor(modules) {
    m = modules;
    l = m.log.register(this);

    m.server.allocateAddress(/^\/api\/.+$/, this.handleEndpoint, "api");

  }

  async handleEndpoint() {

    if (!/^[\w\d/]+$/.test(this.request.url)) {
      return this.respond(400, {
        error: "The request contains invalid characters in the endpoint path"
      });
    }

    const path = this.request.url.split("/");

    // delete "" and "api"
    path.splice(0, 2)

    // if enpoint call was used with / - do remove last empty element
    if (path[path.length - 1] === "") path.pop();

    // get module name, where endpoint should be called
    const moduleName = path.shift();

    // only authed users can access API, except auth module
    if (await this.user.isAnonymous() && moduleName !== "auth") {
      l.debug("Anonymous tried to access private API!");
      return this.respond(401, {
        error: "Unauthorized"
      });
    }

    // do not allow to call raw module
    // do not allow to call internal api/module/object/_ instead of api/module/object/
    if (path.length === 0 ||
      path[path.length - 1] === "$") {
      l.silly("Deny directed access to module internal sources");
      return this.respond(404, {
        error: "Endpoint not exists"
      });
    }

    try {

      // walk over module structure and find property by requested path
      let endpoint = path.reduce((prev, curr) =>
        prev && prev[`$${curr}`], m[moduleName]);

      if (typeof endpoint === "undefined") {
        l.silly(`Endpoint ${this.request.url} not found`);
        return this.respond(404, {
          error: "Endpoint not exists"
        });
      } else {
        l.silly(`Endpoint ${this.request.url} found`);
      }

      if (typeof endpoint === "object") {
        l.silly(`Endpoint is an object, trying to redirect to object facade`);
        if (typeof endpoint["$"] !== "undefined") {
          l.silly(`Object facade found and will be used`);
          endpoint = endpoint["$"];
        } else {
          l.silly(`Object facade not found, deny access`);
          return this.respond(404, {
            error: "Endpoint not exists"
          });
        }
      }

      // since we are 100% shure that endpoint can be executed, we can load
      // some data from client
      const that = this;
      const postOptions = this.request.method == 'POST' ?
        await new Promise((resolve, reject) => {
          // TODO: kill connection if data is downloading too long
          var body = '';
          that.request.on('data', function(data) {
            body += data;
            // Too much POST data, kill the connection!
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6)
              that.request.connection.destroy();
          });
          that.request.on('error', (err) => reject(err));
          that.request.on('end', () => resolve(querystring.parse(body)));
        }) : [];

      l.silly(`Executing endpoint...`);

      // let host = this.request.headers.host;
      //
      // if (host === "leangate-balancer-1") {
      //   host = "leangate-node-1";
      // } else if (host === "leangate-node-1") {
      //   host = "leangate-balancer-1"
      // };
      this.response.setHeader('Access-Control-Allow-Origin', this.request.headers.origin ? this.request.headers.origin : "*");
      this.response.setHeader('Access-Control-Allow-Credentials', "true");

      // functions must be declared via "function", but not via () => {}
      // otherwise context will be lost, more:
      // https://blog.pragmatists.com/the-many-faces-of-this-in-javascript-5f8be40df52e
      return this.respond(
        200,
        typeof endpoint === "function" ? await endpoint.call(this, postOptions) : endpoint
      );

    } catch (err) {
      l.error(`API "${this.request.url}" execution failed - ${err} ${err.stack}`);
      return this.respond(500, {
        error: "An internal API module error has occurred"
      });
    }

  }

}

module.exports = API;
