'use strict';

const fs = require('fs').promises;
const rimraf = require('rimraf');
const crypto = require('crypto');
const {
  SHA3
} = require('sha3');


// TODO: this variables should be more protected, I suppose?
let m;
let l;

class User {

  // TODO: groups - on grants check do check group permission too

  constructor(data) {
    if (typeof data === "number") { // is user remote?

      this.data = {
        id: data
      }
      this.local = false;

      // is user local?
    } else { // is user an object?
      this.data = data;
      this.local = true;
      // TODO: verify loaded user data - may be something broken, old or invalid?
    }
  }

  async set(fieldName, value) {
    if (this.local) {
      if (typeof fieldName === "string") { // get single field
        if (!await m.users.$isSecuredUserField.call(this, {
            field: fieldName
          }))
          this.data[fieldName] = value;
      } else { // get multiple fields
        // TODO: rewrite more efficiently
        for (field in fieldName)
          if (!await m.users.$isSecuredUserField.call(this, {
              field
            }))
            this.data[field] = fieldNames[field];
      }
      this.save();
    } else {
      // TODO remote
    }
  }

  // TODO: reqrite call mathod: no need to do get(["one", "two"])
  // it will be better to do get("one", "two")
  async get(fieldName) {
    if (this.local) {
      if (typeof fieldName === "string") { // get single field
        return !await m.users.$isSecuredUserField.call(this, {
          field: fieldName
        }) ? this.data[fieldName] : undefined;
      } else { // get multiple fields
        let ret = {};
        // TODO: rewrite more efficiently
        for (const field of fieldName)
          // do return value only if it exists
          // this is super effective in case of remote API, to reduce receiving data
          if (typeof this.data[field] !== "undefined" && !await m.users.$isSecuredUserField.call(this, {
              field
            }))
            ret[field] = this.data[field];
        return ret;
      }
    } else {
      // TODO remote
    }
  }

  async save() {
    if (await this.isSystem()) {
      return {
        "error": "System can't be saved"
      };
    }
    return await m.users.$save.call(this, {
      id: this.data.id
    });
  }

  async getUpdated() {
    return await this.get("updated");
  }

  async getCreated() {
    return await this.get("created");
  }

  async getFullName() {
    // TODO: return result according to birth place, becase:
    // Fedotov Leonid Alekseevich is ok in Russia
    // but
    // White Jane is not ok in USA, it should be Jane White
    const {
      firstname,
      secondname,
      lastname,
      fathername
    } = await this.get(["firstname", "secondname", "lastname", "fathername"]);
    return `${lastname}${
      secondname ? " "+secondname : ""
    } ${firstname}${
      fathername ? " "+fathername : ""
    }`;
  }

  async getNames() {
    const {
      firstname,
      secondname
    } = await this.get(["firstname", "secondname"]);
    return `${firstname}${
      secondname ? " "+secondname : ""
    }`;
  }

  async getName() {
    if (await this.isHuman()) {
      return await this.get("firstname");
    } else {
      return await this.get("name");
    }
  }

  async getFirstName() {
    return await this.get("firstname");
  }

  async getSecondName() {
    return await this.get("secondname");
  }

  async getLastName() {
    return await this.get("lastname");
  }

  async getFatherName() {
    return await this.get("fathername");
  }

  async isSuper() {
    return await this.isSystem() ? true : !!(await this.get("grants"))["super"];
  }

  async isAnonymous() {
    return !!this.data.anonymous;
  }

  async isSystem() {
    return this.data.id === 0;
  }

  async isHuman() {
    return (await this.get("type")) === 0;
  }

  async isApp() {
    return (await this.get("type")) === 1;
  }

  async isDevice() {
    return (await this.get("type")) === 2;
  }

  async isMale() {
    return (await this.get("sex")) === 0;
  }

  async isFemale() {
    return (await this.get("sex")) === 1;
  }

  async isLocked() {
    // The magic of Javascript!
    // !!undefined => false
    // !!0 => false
    // !!null => false
    // !!123 => true
    return !!await this.get("locked");
  }

  async canRebootHost() {
    if (await this.isSuper()) return true;
    const grants = await this.get("grants");
    return typeof grants.rebootHost === "boolean" &&
      grants.rebootHost === true
  }

  async canShutdownHost() {
    if (await this.isSuper()) return true;
    const grants = await this.get("grants");
    return typeof grants.shutdownHost === "boolean" &&
      grants.shutdownHost === true
  }

  async canCreateUsers() {
    if (await this.isSuper()) return true;
    const grants = await this.get("grants");
    return typeof grants.createUsers === "boolean" &&
      grants.createUsers === true
  }

  async canEditUsers() {
    if (await this.isSuper()) return true;
    const grants = await this.get("grants");
    return typeof grants.editUsers === "boolean" &&
      grants.editUsers === true
  }

  async canDeleteUsers() {
    if (await this.isSuper()) return true;
    const grants = await this.get("grants");
    return typeof grants.deleteUsers === "boolean" &&
      grants.deleteUsers === true
  }

  async canBrowseUsers() {
    if (await this.isSuper()) return true;
    const grants = await this.get("grants");
    return typeof grants.browseUsers === "boolean" &&
      grants.browseUsers === true
  }

  async canLockUsers() {
    if (await this.isSuper()) return true;
    const grants = await this.get("grants");
    return typeof grants.lockUsers === "boolean" &&
      grants.lockUsers === true
  }

  async lock() {
    await this.set("locked", new Date().getTime());
  }

  async unlock() {
    await this.set("locked", 0);
  }

  async toJson() {
    // TODO: remove temporary/cached data
    return JSON.stringify(this.data);
  }

}

class Users {

  user = User;

  all = [
    new User({
      id: 0,
      created: new Date("2019-05-24T07:10:22Z"),
      updated: new Date("2019-05-24T07:10:22Z"),
      name: "system"
    }),
    new User({
      id: 1,
      created: new Date("2019-05-25T12:58:55Z"),
      updated: new Date("2019-05-25T12:58:55Z"),
      name: "anonymous",
      anonymous: true
    })
  ];

  constructor(modules) {
    m = modules;
    l = m.log.register(this);
  }

  async init() {
    try {
      await fs.access("/data/users");
    } catch (err) {
      await fs.mkdir("/data/users", {
        recursive: true
      });
    }
    try {
      await fs.access("/data/files");
    } catch (err) {
      await fs.mkdir("/data/files");
    }
    await this.loadAll();
  }

  async $save({
    id
  }) {
    // TODO: security
    if (typeof id !== "number" || id < 1) {
      if (this.ctx && this.isCausedByRequest())
        this.respondCode = 406;
      return {
        "error": "Invalid ID value"
      };
    };
    const user = await m.users.findById(id);
    l.silly(`Saving user #${user.data.id} account...`);
    // is there user directory?
    const userDirectory = `/data/users/${user.data.id}`;
    try {
      await fs.access(userDirectory);
    } catch (err) {
      l.silly(`User #${user.data.id} is a new user, a directory will be created`);
      await fs.mkdir(userDirectory);
    }
    try {
      await fs.writeFile(`${userDirectory}/account.json`, await user.toJson());
      l.silly(`User #${user.data.id} account saved`);
    } catch (err) {
      l.error(`Failed to save #${user.data.id} user! ${err} ${err.stack}`);
      throw err;
    }
  }

  async loadAll() {
    if (this.all.length > 2) return; // do not load if already loaded
    const usersFiles = await fs.readdir('/data/users');
    l.debug(`Found ${usersFiles.length} user${usersFiles.length===1 ? "" : "s"} in system`);
    for (const userFileName of usersFiles) {
      l.silly(`Loading #${userFileName} user file`);
      try {
        const userFileData = require(`/data/users/${userFileName}/account.json`);
        this.all.push(new User(userFileData));
        l.silly(`File #${userFileName} loaded`);
      } catch (err) {
        l.error(`Failed to load #${userFileName} user file! ${err} ${err.stack}`);
      }
    }
    l.info(`There ${this.all.length===1 ? "is" : "are"} ${this.all.length} loaded user${this.all.length===1 ? "" : "s"} in system`);
  }

  $version = "1.0.0"

  $userFieldsSortPriority = [
    "id",
    "username",
    "type",
    "firstname",
    "name",
    "lastname",
    "secondname",
    "fathername",
    "created",
    "sex",
    "updated"
  ];

  $userGrantsList = [
    "super",
    "rebootHost",
    "shutdownHost",
    "createUsers",
    "editUsers",
    "deleteUsers",
    "browseUsers",
    "lockUsers"
  ]

  $me = {
    $: async function({
      fields
    }) {
      if (typeof fields !== "string") {
        fields = [
          "id",
          "name",
          "username",
          "firstname",
          "secondname",
          "lastname",
          "fathername",
          "sex",
          "type",
          "grants",
          "updated",
          "created"
        ];
      } else {
        fields = fields.split(",");
      }
      return await this.user.get(fields);
    },
    $id: async function() {
      return await this.user.get(["id"]);
    },
    $username: async function() {
      return await this.user.get(["username"]);
    }
  }

  async $isSecuredUserField({
    field
  }) {
    return !field ? false : [
      "secret",
      "secretAlgorithm",
      "updated",
      "created"
    ].includes(field);
  }

  async $createUser(options) {

    if (this.ctx)
      if (!await this.user.canCreateUsers()) {
        if (this.isCausedByRequest()) this.respondCode = 403;
        return {
          error: "Access denied"
        }
      }

    let user = {
      // todo: ask balancer if id already exists
      // we can register 8,999,999,999,999,999 users
      // 8 because there will be no users between 0 - 1000000000000000
      id: Math.floor(Math.random() * 1000000000000000),
      created: new Date().getTime(),
      updated: 0
    };

    // no need to define type 0 if username already defined
    if (typeof typeof options["username"] === "string") {
      options["type"] = 0;
    }
    if (typeof options["type"] === "string") {
      options["type"] = parseInt(options["type"]);
    }

    if (options["type"] > 3 || options["type"] < 0 || isNaN(options["type"])) {
      if (this.ctx && this.isCausedByRequest())
        this.respondCode = 406;
      return {
        error: "Invalid type value"
      }
    }

    if (typeof options["username"] !== "string") {
      if (this.ctx && this.isCausedByRequest())
        this.respondCode = 400;
      return {
        error: "No username specified"
      }
    }

    if (typeof options["password"] !== "string") {
      if (this.ctx && this.isCausedByRequest())
        this.respondCode = 400;
      return {
        error: "No password specified"
      }
    }

    if (typeof options["type"] === "number") {
      user["type"] = options["type"];
    }

    if (typeof options["grants"] !== "string" && typeof options["grants"] !== "object") {
      user["grants"] = {
        browseUsers: true
      }
    }

    if (typeof options["sex"] !== "string" && typeof options["sex"] !== "number") {
      user["sex"] = 0;
    }

    options["userObject"] = new User(user);

    const edited = await m.users.$editUser.call(this, options);
    if (!(edited instanceof User)) return edited;

    m.users.all.push(edited);
    const saveError = await edited.save();
    return saveError ? saveError : {
      id: edited.data.id
    }
  }

  async $editUser(options) {

    let user;

    if (typeof options["userObject"] === "object") {
      // no need to verify access rights - everything edited in memory only
      user = options["userObject"];
    } else {

      // there is no option to edit user by username,
      // because at the time of editing it is impossible to say for sure
      // that we are editing someone we expect

      let {
        id
      } = options;

      if (this.ctx)
        if (!await this.user.canEditUsers()) {
          if (this.isCausedByRequest()) this.respondCode = 403;
          return {
            error: "Access denied"
          }
        }

      if (typeof id === "string") {
        id = parseInt(id);
      }
      if (typeof id !== "number" || isNaN(id) || id < 1) {
        if (this.ctx && this.isCausedByRequest()) this.respondCode = 406;
        return {
          error: "Invalid ID value"
        }
      }

      user = await m.users.findById(id);

      if (typeof user === "undefined") {
        if (this.ctx && this.isCausedByRequest()) this.respondCode = 422;
        return {
          error: "User not found"
        }
      }

    }

    if (user.data.type === 0) {
      if (typeof options["username"] === "string" && options["username"] !== user.data.username) {
        if (options["username"].length < 5) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Username is too short"
          }
        }
        if (options["username"].length > 16) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Username is too long"
          }
        }
        if (!/^[\w\d]+$/.test(options["username"])) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Username contains invalid characters"
          }
        }
        // todo: ask balancer
        const found = await m.users.findByUsername(options["username"]);
        if (found) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Username already taken"
          }
        }
        user.data.username = options["username"];
      }
      if (typeof options["firstname"] === "string" && options["firstname"] !== user.data.firstname) {
        if (options["firstname"].length < 2) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Firstname is too short"
          }
        }
        if (options["firstname"].length > 16) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Firstname is too long"
          }
        }
        if (!/^[a-zA-Zа-яА-Я]+$/.test(options["firstname"])) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Firstname contains invalid characters"
          }
        }
        user.data.firstname = options["firstname"];
      }
      if (typeof options["lastname"] === "string" && options["lastname"] !== user.data.lastname) {
        if (options["lastname"].length < 2) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Lastname is too short"
          }
        }
        if (options["lastname"].length > 16) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Lastname is too long"
          }
        }
        if (!/^[a-zA-Zа-яА-Я]+$/.test(options["lastname"])) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Lastname contains invalid characters"
          }
        }
        user.data.lastname = options["lastname"];
      }
      if (typeof options["sex"] === "string") {
        if (options["sex"] === "male") {
          options["sex"] = 0;
        } else if (options["sex"] === "female") {
          options["sex"] = 1;
        } else {
          options["sex"] = parseInt(options["sex"]);
        }
      }
      if (typeof options["sex"] === "number" && options["sex"] !== user.data.sex) {
        if (isNaN(options["sex"]) || options["sex"] > 1 || options["sex"] < 0) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Invalid sex value"
          }
        }
        user.data.sex = options["sex"];
      }
      if (typeof options["password"] === "string") {
        if (options["password"].length < 8) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Password is too short"
          }
        }
        if (options["password"].length > 32) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Password is too long"
          }
        }
        const hash = new SHA3(512);
        hash.update(options["password"]);
        user.data.secret = hash.digest('hex');
        user.data.secretAlgorithm = 1;
      }
      if (typeof options["grants"] === "string") {
        try {
          options["grants"] = JSON.parse(options["grants"]);
        } catch (err) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 406;
          return {
            error: "Invalid grants value"
          }
        }
      }
      if (typeof options["grants"] === "object") {

        // if you not a super, you can't change grants
        if (!await this.user.isSuper()) {
          if (this.ctx && this.isCausedByRequest())
            this.respondCode = 403;
          return {
            error: "Non-super trying to change permissions"
          }
        }

        const grantsKeys = await m.users.$filterUserGrants.call(this, {
          grants: Object.keys(options["grants"])
        });
        if (!(grantsKeys instanceof Array)) return grantsKeys;
        if (typeof user.data["grants"] === "undefined") user.data["grants"] = {};
        for (const key of grantsKeys) {
          if (options["grants"][key] === false) {
            // is there anything to delete?
            if (typeof user.data.grants[key] === "boolean")
              delete user.data.grants[key];
          } else {
            user.data.grants[key] = true;
          }
        }
      }
    } else {
      // TODO: support apps and devices
      return;
    }

    if (typeof options["userObject"] === "object") {
      return user;
    } else {
      const saveError = await user.save();
      return saveError ? saveError : {}
    }

  }

  async $deleteUser({
    id
  }) {

    if (this.ctx)
      if (!await this.user.canDeleteUsers()) {
        if (this.ctx && this.isCausedByRequest()) this.respondCode = 403;
        return {
          error: "Access denied"
        }
      }


    // there is no option to delete user by username,
    // because at the time of deletion it is impossible to say for sure
    // that we are deleting someone we expect

    if (typeof id === "string") {
      id = parseInt(id);
    }
    if (typeof id !== "number" || isNaN(id) || id < 1) {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 406;
      return {
        error: "Invalid user ID value"
      }
    }

    if (id === this.user.data.id) {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 422;
      return {
        error: "Not possible to delete yourself"
      }
    }

    for (var i = 0; i < m.users.all.length; i++) {
      if (m.users.all[i].data.id === id) {

        if (await m.users.all[i].isSystem()) {
          if (this.ctx && this.isCausedByRequest()) this.respondCode = 403;
          return {
            error: "Access denied"
          }
        }

        // todo: destroy all user files
        // todo: destroy user session!
        // todo: remove user from caches
        m.users.all.splice(i, 1);

        try {
          rimraf.sync(`/data/users/${id}`);
        } catch (err) {
          l.error(`Not possible to delete user #${id} directory! ${err} ${err.stack}`);
          throw err;
        }
        // todo: return how much space freed up
        return {};
      }
    }

    if (this.ctx && this.isCausedByRequest()) this.respondCode = 422;
    return {
      error: "User not found"
    }

  }

  async $sortUserFields({
    fields
  }) {

    // no need to verify something, everything will be verified in filter
    let filteredFields = await m.users.$filterUserFields.call(this, {
      fields
    });

    // is filter returned an error?
    if (!(filteredFields instanceof Array)) return filteredFields;

    if (filteredFields.length < 1) return [];

    filteredFields.sort((a, b) => m.users.$userFieldsSortPriority.indexOf(a) - m.users.$userFieldsSortPriority.indexOf(b));

    return filteredFields;

  }

  async $filterUserFields({
    fields
  }) {

    if (typeof fields === "string") fields = fields.split(",");
    if (typeof fields !== "object" || !(fields instanceof Array)) {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 406;
      return {
        error: "Invalid fields value"
      }
    }

    if (fields.length < 1) return [];

    return fields.filter(field => m.users.$userFieldsSortPriority.includes(field));

  }

  async $filterUserGrants({
    grants
  }) {

    if (typeof grants === "string") grants = grants.split(",");
    if (typeof grants !== "object" || !(grants instanceof Array)) {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 406;
      return {
        error: "Invalid grants value"
      }
    }

    if (grants.length < 1) return [];

    return grants.filter(grant => m.users.$userGrantsList.includes(grant));

  }

  async $findUsers(options) {

    l.silly("Users search started")

    if (this.ctx)
      if (!await this.user.canBrowseUsers()) {
        l.silly("Search denied: no grants");
        if (this.isCausedByRequest()) this.respondCode = 403;
        return {
          error: "Access denied"
        }
      }

    const fields = await m.users.$sortUserFields.call(this, {
      fields: Object.keys(options)
    });

    if (!(fields instanceof Array)) return fields;

    if (fields.length < 1) {
      l.silly("Search cancelled: after filter there are no fields to search");
      return [];
    }

    const nonEmptyFields = fields.filter(field => !!options[field]);

    if (nonEmptyFields.length < 1) {
      l.silly("Search cancelled: there are no non-empty fields");
      return [];
    }

    let results = m.users.all;

    l.silly(`Performing search on ${nonEmptyFields.length} field${
nonEmptyFields.length===1?"":"s"}`);

    // do not do search on field if its value is empty - nothing to compare
    for (const field of nonEmptyFields) {

      let preresults = [];
      // convert to string because somebody can try to use non-string value
      options[field] = new RegExp(options[field].toString(), "i");

      for (const user of results)
        if (typeof user.data[field] !== "undefined" &&
          options[field].test(user.data[field].toString())) preresults.push(user);

      results = preresults;

    }

    l.silly(`Search done, found ${results.length} result${
results.length===1?"":"s"}`);

    if (this.ctx && this.isCausedByRequest()) {
      let finalResults = [];
      for (const user of results) {
        let clearUser = {};
        // but include empty field,
        // in case if caller wants to get some specific user's profile data
        for (const field of fields) {
          clearUser[field] = user.data[field];
        }
        finalResults.push(clearUser);
      }
      return finalResults;
    } else {
      return results;
    }

  }

  async $getUser({
    id,
    username
  }) {

    if (typeof id === "undefined" && typeof username !== "string") {
      if (this.isCausedByRequest()) this.respondCode = 406;
      return {
        error: "Neither ID nor username is not specified"
      }
    }

    let query = {};

    if (typeof id !== "undefined") {
      if (typeof id === "string") id = parseInt(id);
      if (isNaN(id) || id < 1) {
        if (this.isCausedByRequest()) this.respondCode = 406;
        return {
          error: "Invalid user ID value"
        }
      }
      query["id"] = `^${id}$`; // search for entire id match, but not for it's part
    } else {
      if (username.length < 5) {
        if (this.ctx && this.isCausedByRequest())
          this.respondCode = 406;
        return {
          error: "Username is too short"
        }
      }
      if (username.length > 16) {
        if (this.ctx && this.isCausedByRequest())
          this.respondCode = 406;
        return {
          error: "Username is too long"
        }
      }
      if (!/^[\w\d]+$/.test(username)) {
        if (this.ctx && this.isCausedByRequest()) this.respondCode = 406;
        return {
          error: "Username contains invalid characters"
        }
      }
      query["username"] = `^${username}$`; // search for entire username match, but not for it's part
    }

    // add other fields to return in result
    for (const field of m.users.$userFieldsSortPriority)
      if (typeof query[field] === "undefined")
        query[field] = "";

    const users = await m.users.$findUsers.call(this, query);
    return users.length > 0 ? users[0] : {};

  }

  // TODO: make caches on load, to force search
  async findBy(field, value) {
    // todo: do this asynchroniously
    for (const user of m.users.all) {
      if (user.data[field] === value) return user;
    }
    return null;
  }

  async findById(id) {
    return this.findBy("id", id);
  }

  async findByUsername(username) {
    return this.findBy("username", username);
  }

  async loginUser(id, username, nonSecuredSecret) {

    // todo: IMPORTANT! Set time when next login is possible to avoid bruteforce
    // and fail the connection if there was less than 1 seconds until next login
    // TODO: on many attempts do lock account
    // TODO: on many password failures do lock account

    const user = typeof id === "undefined" ?
      await this.findByUsername(username) :
      await this.findById(id);

    // todo: fix this
    if (typeof user !== "object" || !user || await user.isSystem() || await user.isAnonymous()) return false;

    l.silly(`Requested user #${user.data.id} found`);

    let secret;
    switch (user.data.secretAlgorithm) {
      case 0: // sha-512
        secret = crypto.createHash('sha512').update(nonSecuredSecret).digest('hex');
        break;
      case 1: // sha3-512
        const hash = new SHA3(512);
        hash.update(nonSecuredSecret);
        secret = hash.digest('hex');
        break;
      default:
        l.error(`Unknown secret algorithm "${user.data.secretAlgorithm}" found for \
user "${user.data.id}"`);
        // May be do not show server failure, but do
        throw new Error("Unknown secret algorithm");
    }
    return user.data.secret === secret ? user : false;
  }

}

module.exports = Users;
