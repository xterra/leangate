'use strict';

const http = require('http');

let m;
let l;

class Monitor {

  metrics = null;
  metricsUpdated = 0;

  constructor(modules) {
    m = modules;
    l = m.log.register(this);
  }

  async init() {
    l.debug("Starting watchdog internal server...");
    return new Promise((resolve, reject) => {
      m.monitor.watchdogServer = http.createServer(this.handle);
      m.monitor.watchdogServer.listen(8082, (err) => {
        if (err) {
          l.error("Not possible to start watchdog server");
          reject(err);
        }
        l.info("Watchdog server started");
        resolve();
      });
    });
  }

  handle(request, response) {

    var body = '';

    request.on('data', function(data) {
      body += data;
      // Too much POST data, kill the connection!
      // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
      if (body.length > 1e6)
        request.connection.destroy();
    });
    request.on('error', (err) => {
      l.error("Failed to download POST data");
    });
    request.on('end', () => {
      m.monitor.storeHostMetrics({
        metrics: JSON.parse(body)
      });
    });

  }

  storeHostMetrics({
    metrics
  }) {
    this.metrics = metrics;
    this.metricsUpdated = new Date().getTime();
  }

  $version = "1.0.0"

}

module.exports = Monitor;
