'use strict';

// Typical modules loading sequence:
// 1. log
// 2. server
// 3. api
// 4. users
// 5. system
// 6. monitor

const modulesConfigs = require("/leangate/modules.json");
console.log("Modules", modulesConfigs);
let modules = {};
(async () => {
  while (Object.keys(modules).length < modulesConfigs.length) {
    for (let i = 0; i < modulesConfigs.length; i++) {
      const moduleConfig = modulesConfigs[i];
      if (typeof modules[moduleConfig["name"]] === "undefined") {
        let doLoad = true;
        if (typeof moduleConfig["after"] !== "undefined") {
          for (let j = 0; j < moduleConfig["after"].length; j++) {
            if (typeof modules[moduleConfig["after"][j]] === "undefined") {
              doLoad = false;
            }
          }
        }
        if (doLoad) {
          modules[moduleConfig["name"]] =
            new(require(`./${moduleConfig["name"]}.js`))(modules);
          if (typeof modules[moduleConfig["name"]]["init"] === "function") {
            await modules[moduleConfig["name"]]["init"]();
          }
        }
      }
    }
  }
})().then(_ => {
  // TODO: do something here?
}).catch(error => {
  console.error("An error occurred on modules load", error);
  process.exit(1);
});
