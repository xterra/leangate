'use strict';

const http = require("http");
const httpPort = 8080;
const httpsPort = 8443;

let m;
let l;

class Server {

  server = null;

  constructor(modules) {
    m = modules;
    l = m.log.register(this);
  }

  async init() {
    l.debug("Starting HTTP server...");
    return new Promise((resolve, reject) => {
      m.server.server = http.createServer(this.handle);
      m.server.server.listen(httpPort, (err) => {
        if (err) {
          l.error("Not possible to start HTTP server");
          reject(err);
        }
        l.info("HTTP server started");
        resolve();
      });
    });
  }

  addressRulePool = [];

  allocateAddress(addressRegexp, receiver, name) {
    l.silly(`Trying to allocate "${addressRegexp}" address...`);
    if (this.addressRulePool.filter((value) => {
        return value.regexp.source === addressRegexp.source &&
          value.regexp.flags.split("").sort().join("") ===
          address.flags.split("").sort().join("");
      }).length > 0) {
      l.error(`Failed to allocate "${addressRegexp}" address - \
already allocated!`);
      throw Error("Adress already allocated");
    }
    this.addressRulePool.push({
      regexp: addressRegexp,
      name: name,
      receiver: receiver
    });
    l.silly(`Address "${addressRegexp}" allocated`);
  }

  deallocateAddress(address) {
    l.silly(`Deallocating "${addressRegexp}" address...`);
    this.addressRulePool = this.addressRulePool.filter((value) => {
      return !(value.regexp.source === address.source &&
        value.regexp.flags.split("").sort().join("") ===
        address.flags.split("").sort().join(""));

    });
  }

  handle(request, response) {

    (async () => {

      let found;
      for (const allocation of m.server.addressRulePool) {
        if (allocation.regexp.test(request.url)) {
          found = allocation;
          break;
        }
      }

      const { user, session } = await m.auth.identify(request, response);

      const context = m.context.new({
        user: user,
        session: session,
        request: request,
        response: response,
        endpoint: found ? found.name : null
      });

      if (found) {
        found.receiver.call(context).catch(error => {
          l.error(`Receiver failed - ${error} ${error.stack}`);
          context.respond(500, {
            error: "Internal server error"
          });
        });
      } else {
        context.respond(404, {
          error: "Not Found"
        });
      }

    })().catch(error => {
      l.error(`Unhandled exception - ${error} ${error.stack}`);
      response.writeHead(500);
      response.end("Internal server error");
    });

  }

}

module.exports = Server;
