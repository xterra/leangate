'use strict';

const fs = require('fs');
const path = require('path');
const mime = require('mime-types');

let m;
let l;

class Horizon {

  constructor(modules) {
    m = modules;
    l = m.log.register(this);

    // Handle:
    // "/"
    // "/index.html"
    // "/horizon/*"
    m.server.allocateAddress(/^\/(|index.html|horizon.*)$/, this.handleEndpoint, "horizon");

  }

  async handleEndpoint() {

    const url = decodeURI(this.request.url);

    // is there bad symbold in url?
    if (!/^[\w\d/.@-]+$/.test(url)) {
      l.debug(`${this.request.connection.remoteAddress} <- 404 Not found, ${url} file failed bad symbols test`);
      this.response.writeHead(404);
      return this.response.end("Not Found");
    }

    // forward index aliases to root index file
    if (/^\/(index.html|horizon(|\/|\/index.html))$/.test(url)) {
      l.debug(`${this.request.connection.remoteAddress} <- 301 Redirect to root index file`);
      this.response.setHeader('Location', '/');
      this.response.writeHead(301);
      return this.response.end();
    }

    const normalizedPath = path.normalize(url).split("/horizon/");

    // if root, then send index file
    // otherwise send requested file
    const filePath = url === "/" ?
      "/horizon/index.html" :
      !!normalizedPath[1] ? `/horizon/${normalizedPath[1]}` : null

    if (!filePath) {
      l.debug(`${this.request.connection.remoteAddress} <- 404 Not found ${url} file`);
      this.response.writeHead(404);
      return this.response.end("Not Found");
    }

    let stat;

    try {

      await fs.promises.access(filePath, fs.constants.F_OK);

      stat = await fs.promises.stat(filePath);

      if(!stat.isFile()) throw new Error("Not a file");

    } catch (err) {
      l.debug(`${this.request.connection.remoteAddress} <- 404 Not found ${url} file`);
      this.response.writeHead(404);
      return this.response.end("Not Found");
    }

    let contentType = mime.lookup(filePath);

    if (!contentType) contentType = "application/octet-stream";

    l.debug(`${this.request.connection.remoteAddress} <- 200 OK ${contentType} ${stat.size}B ${filePath}`);

    this.response.writeHead(200, {
      'Content-Type': contentType,
      'Content-Length': stat.size
    });

    try{
      fs.createReadStream(filePath).pipe(this.response);
    } catch (err) {
      l.error(`Readstream failure ${err} ${err.stack}`);
    }


  }

}

module.exports = Horizon;
