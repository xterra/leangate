'use strict';

const querystring = require('querystring');
const http = require('http');
const fs = require('fs').promises;

// TODO: this variable should be more protected, I suppose?
let m;
let l;

class System {

  token = null;
  nodes = {};
  clusterMetrics;

  constructor(modules) {
    m = modules;
    l = m.log.register(this);
  }

  async init() {
    m.system.nodes = require(`/configs/nodes.json`);
    m.system.token = (await fs.readFile(`/configs/secret.key`, "utf8")).replace(/(\r\n|\n|\r)/gm, "");
    l.silly("System secret token loaded!");

    // setInterval(() => {
    //   l.silly("Collecting cluster metrics...");
    //   m.system.$getClusterMetrics.call({
    //     user: {
    //       data: {
    //         id: 0
    //       }
    //     },
    //     session: {
    //       sessid: 0
    //     }
    //   }).then(metrics => m.system.clusterMetrics = metrics).catch(err => {
    //     l.error(`An error occurred during cluster metrics load ${err} ${err.stack}`);
    //   });
    // }, 5000)

  }

  async perform({
    node,
    endpoint,
    parameters
  }) {

    l.silly(`Performing endpoint "${endpoint}" call to "${node}" node`);

    const data = typeof parameters !== "object" ? "" : querystring.stringify(parameters);

    return new Promise((resolve, reject) => {

      const request = http.request({
        host: node,
        port: '80',
        path: `/api/${endpoint}`,
        method: typeof parameters !== "object" ? "GET" : 'POST',
        headers: {
          'X-Token': m.system.token,
          'X-User': this.user.data.id.toString(),
          'X-SESSID': this.session.sessid.toString(),
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': Buffer.byteLength(data)
        }
      }, function(res) {
        l.silly("Got remote system respond!");
        if (res.statusCode !== 200) {
          l.silly(`Remote call returned negative code ${res.statusCode}`);
          return reject({
            respond: res,
            data: null
          });
        }
        res.setEncoding('utf8');
        res.on('data', function(chunk) {
          try {
            let parsed = JSON.parse(chunk);
            return resolve({
              respond: res,
              data: parsed
            });
          } catch (err) {
            l.error(`Failed to parse remote system response ${err} ${err.stack}`);
            return reject(err);
          }
        });
      }).on("error", (err) => {
        return reject(err);
      });

      l.silly("Performing call...");

      request.write(data);
      request.end();

    });

  }

  async $getHostMetrics() {
    // TODO: security!
    return {
      metrics: m.monitor.metrics,
      updated: m.monitor.metricsUpdated
    };
  }

  async $getClusterMetrics() {

    // TODO: security!

    let metrics = [];
    for (const node of m.system.nodes) {
      l.silly(`Getting metrics from ${node.address}`);
      let {
        data
      } = await m.system.perform.call(this, {
        node: node.address,
        endpoint: "system/getHostMetrics"
      });
      data.node = node.address;
      metrics.push(data);
    }

    l.silly("Metrics data loaded from all nodes");

    return metrics;

  }

}

module.exports = System;
