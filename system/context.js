'use strict';

const uuidv4 = require('uuid/v4');
const accepts = require('accepts');
const yaml = require('js-yaml');
const xml = require('xml-js');

// TODO: this variable should be more protected, I suppose?
let m;
let l;

class Context {

  ctx = true;

  session = null;
  user = null;
  request = null;
  response = null;
  parent = null;

  constructor(options) {
    if (typeof options["log"] === "object") {

      m = options;
      l = m.log.register(this);

    } else {

      this.id = uuidv4();

      if (typeof options.user === "object" && options.user)
        this.user = options.user;

      if (typeof options.session === "object" && options.session)
        this.session = options.session;

      if (typeof options.request === "object" && options.request) {
        this.request = options.request;
        this.accepts = accepts(this.request);
      }

      if (typeof options.response === "object" && options.response)
        this.response = options.response;

      if (typeof options.parent === "object" && options.parent) {

        // todo: check user grants from parent. Case: when user from parent has
        // less grants, then user from child - grants can't be expanded!

        this.parent = options.parent;

        this.request = this.parent.request;
        this.response = this.parent.response;

        if (!this.user)
          this.user = this.parent.user;

      } else if (this.request !== null) {
        l.debug(`${this.request.connection.remoteAddress} \
${this.request.method} ${this.request.url} -> ${
  options.endpoint ? options.endpoint : "nowhere"
}`);
      }

    }
  }

  new(options) {
    // todo: handle case when m.context.new() called
    // instead of m.context.new({}) - something wrong can happen
    return new Context(typeof options === "undefined" ? {
      parent: this
    } : options);
  }

  isCausedByRequest() {
    return this.request !== null;
  }

  isChild() {
    return this.parent !== null;
  }

  getShortID() {
    return this.id.substring(0, 8);
  }

  async respond(code, rawData) {

    l.silly("Respond called");

    if (typeof rawData === "undefined" || rawData == null) {
      l.silly("Data is empty, nothing to send to client");
      return this.send(code, null);
    }

    let data;

    // respond supports:
    // - json (default)
    // - xml
    // - yaml
    const contentType = this.accepts.type([
      'html',
      'json',
      'xml',
      'application/x-yaml'
    ]);
    l.silly(`Client requested "${contentType}" content-type`);

    switch (contentType) {
      case 'xml':
        if (typeof rawData !== "object") {
          rawData = {
            data: rawData
          }
        }
        data = xml.js2xml(rawData, {
          compact: true,
          spaces: 0
        });
        this.response.setHeader('Content-Type', 'application/xml')
        break;
      case "application/x-yaml":
        data = yaml.dump(rawData);
        this.response.setHeader('Content-Type', 'application/x-yaml')
        break;
      default:
        if (typeof rawData !== "object") {
          rawData = {
            data: rawData
          }
        }
        data = JSON.stringify(rawData);
        this.response.setHeader('Content-Type', 'application/json')
        break;
    }

    // TODO: form data according to type from context
    return this.send(code, data);

  }

  async send(code, data) {
    if (this.response.finished) {
      l.error(`Can't write data to response: response already sended`);
      return;
    }
    if (l.isDebug())
      l.debug(`${this.request.connection.remoteAddress} \
<- ${code} ${!data ? 0 : Buffer.byteLength(data, 'utf8')}B`);
    if (code === 200 && typeof this.respondCode === "number")
      code = this.respondCode;
    this.response.writeHead(code);
    if (data) {
      this.response.write(data);
    }
    this.response.end();
  }

}

module.exports = Context;
