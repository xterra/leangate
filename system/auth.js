'use strict';

const fs = require('fs').promises;
const uuidv4 = require('uuid/v4');

// TODO: this variables should be more protected, I suppose?
let m;
let l;

// !!!
// AUTH MODULE SHOULD BE MOST SECURE MODULE
// BECAUSE IT CAN BE ACCESSED BY ANONYMOUS USERS
// !!!

class Session {

  constructor({
    sessid,
    id,
    created,
    prolonged,
    remote
  }) {

    if (typeof created === "undefined") {
      this.created = new Date().getTime();
    } else {
      this.created = created;
    }
    if (typeof prolonged === "number" && prolonged > this.created)
      this.prolonged = prolonged;

    this.remote = typeof remote === "boolean" ? remote : false;

    this.sessid = sessid;
    this.id = id;

  }

  async save() {

    // TODO: save every 5 seconds

    if (this.remote) return;

    const path = `/data/sessions/${this.sessid}`;
    const time = new Date(this.getLatestTime());

    // save id on disk
    await fs.writeFile(path, this.id);

    // write prolongation time
    await fs.utimes(path, time, time);

  }

  async delete() {

    if (this.remote) return;

    // delete from disk
    await fs.unlink(`/data/sessions/${this.sessid}`);

    // delete from memory
    m.auth.sessions.delete(this.sessid);

  }

  async prolong() {
    this.prolonged = new Date().getTime();
    return await this.save();
  }

  getLatestTime() {
    return typeof this.prolonged === "number" && this.prolonged > this.created ?
      this.prolonged :
      this.created
  }

  getExpirationTime() {
    if(this.remote) return new Date().getTime() + m.auth.sessionLifetime; // TODO: fix this hack
    return this.getLatestTime() + m.auth.sessionLifetime;
  }

  isOutdated() {
    return this.getExpirationTime() < new Date().getTime()
  }

}

class Auth {

  maxMemoryStoredSessions = 100000; // TODO
  sessionLifetime = 172800000; // 2 days in milliseconds : 2 * 24 * 60 * 60 * 1000
  sessionsCleanupInterval = 300000; // every 5 minutes (5 * 60 * 1000)
  sessions = new Map();

  constructor(modules) {

    m = modules;
    l = m.log.register(this);

  }

  $version = "1.0.0"

  async init() {
    try {
      await fs.access("/data/sessions");
    } catch (err) {
      await fs.mkdir("/data/sessions");
    }
    setInterval(this.cleanup, this.sessionsCleanupInterval);
    this.cleanup();
  }

  async cleanup() {
    l.debug("Sessions cleanup started");
    let cleaned = 0;
    try {

      // first of all, let's cleanup everything in memory
      // TODO: this is non-optimal solution
      for (const session of m.auth.sessions.entries())
        if (session[1].isOutdated()) {
          // delete asyncroniously, no need to wait
          // TODO: catch
          session[1].delete();
          cleaned++;
        }

      // then, let's cleanup everything else on disk
      const files = await fs.readdir("/data/sessions");

      if (files.length > 0) {

        // no need to calculate time on each iteration
        const currTime = new Date().getTime();

        for (const file of files) {

          const fileName = `/data/sessions/${file}`

          const {
            mtime,
            ctime
          } = await fs.stat(fileName);

          const created = new Date(ctime).getTime();
          const prolonged = new Date(mtime).getTime();

          const latestTime = prolonged > created ? prolonged : created;

          if (latestTime + m.auth.sessionLifetime < currTime) {
            const session = m.auth.sessions.get(file);
            if (typeof session !== "undefined") {
              session.delete();
            } else {
              fs.unlink(fileName);
            }
            cleaned++;
          }

        }

      }

      l.debug(`Sessions cleanup ended, cleaned: ${cleaned}`);

    } catch (err) {
      l.error(`Cleanup failed ${err} ${err.stack}`);
    }
  }

  parseCookies(cookies) {
    // Thank u https://stackoverflow.com/a/3409200
    let list = {};
    if (cookies)
      cookies.split(';').forEach(function(cookie) {
        let parts = cookie.split('=');
        list[parts.shift().trim()] = decodeURI(parts.join('='));
      });
    return list;
  }

  async identify(request, response, forceSessid) {

    if (typeof request.headers["x-token"] === "string") {
      l.silly("System-to-system call found");
      if (request.headers["x-token"] === m.system.token) {
        l.silly("System authed!");
        const id = parseInt(request.headers["x-user"]);
        let user;
        const users = await m.users.$findUsers({
          id
        });
        if (users.length < 1) {
          l.silly("Remote user is not in memory yet")
          user = new(m.users.user)({
            id,
            remote: true,
            node: null // TODO
          });
          m.users.all.push(user);
        } else {
          user = users[0];
        }
        let foundSession = m.auth.sessions.get(request.headers["x-sessid"]);
        if (!foundSession) {
          l.silly("Remote session is not found yet, let's create...");
          const session = new Session({
            sessid: request.headers["x-sessid"],
            id: user.data.id,
            remote: true
          });
          m.auth.sessions.set(session.sessid, session);
          l.silly("Remote session created!");
          foundSession = session;
        }
        return {
          foundSession,
          user
        };
      } else {
        l.error("Invalid system-to-system auth occurred!");
      }
    }

    const cookies = m.auth.parseCookies(request.headers.cookie);

    const sessid = forceSessid || cookies["SESSID"];

    try {

      if (typeof sessid !== "string") throw Error("No session cookie");

      const sessionFromMemory = m.auth.sessions.get(sessid);

      // on $findUsers call
      // there is not context to provide, then, do not provide it!
      // if no context provided, then real User objects will be returned
      // $findUsers always return an array!

      // is session already loaded to memory?
      if (typeof sessionFromMemory !== "undefined") {
        l.silly(`Session ${sessid} found in memory`);

        // is session is still actual?
        if (sessionFromMemory.isOutdated()) {
          sessionFromMemory.delete();
          throw Error("Session expired");
        }

        // is user still exists?
        const users = (await m.users.$findUsers({
          id: sessionFromMemory.id
        }));
        if (users.length < 1) throw Error("User not exists");

        // await prolongation, because following code can delete session,
        // but prolongation will not be ready and error will occurr
        await sessionFromMemory.prolong();

        // let's refresh cookie on client
        if (response) response.setHeader("Set-Cookie", `SESSID=${sessionFromMemory.sessid}; Path=/; Expires=${new Date(sessionFromMemory.getExpirationTime()).toGMTString()}`);

        return {
          session: sessionFromMemory,
          user: users[0]
        }
      }

      l.silly(`Session ${sessid} not found in memory`);

      // if session is not in memory, let's try to find it on disk

      // read modification time and creation time
      const {
        mtime,
        ctime
      } = await fs.stat(`/data/sessions/${sessid}`);

      const created = new Date(ctime).getTime();
      const prolonged = new Date(mtime).getTime();

      // read id from file
      const id = parseInt(await fs.readFile(`/data/sessions/${sessid}`, "utf8"));
      if (isNaN(id)) throw Error(`User ID "${id}" is currepted`);

      // let's create session object
      const session = new Session({
        sessid,
        id,
        created,
        prolonged
      });

      // is session is still actual?
      if (session.isOutdated()) {
        session.delete();
        throw Error("Session expired");
      }

      // is user still exists?
      const users = (await m.users.$findUsers({
        id
      }));
      if (users.length < 1) {
        // user not exists, then session file is no longer required
        session.delete();
        throw Error("User not exists");
      }

      // seems everything ok, do prolong session
      await session.prolong();

      const user = users[0];

      // put session object to memory
      m.auth.sessions.set(sessid, session);

      // let's refresh cookie on client
      if (!!response) response.setHeader("Set-Cookie", `SESSID=${session.sessid}; Path=/; Expires=${new Date(session.getExpirationTime()).toGMTString()}`);

      return {
        session,
        user
      };

    } catch (err) {

      // session file not found, seems no session exists at all
      // or session expired
      // or user not exists
      // or something bad happened
      l.silly(`Access denied`)

      // destroy cookie on client only if invalid cookie was sent
      if (typeof sessid === "string" && !!response) response.setHeader("Set-Cookie", `SESSID=; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`);

      return {
        session: null,
        user: (await m.users.$findUsers({
          id: 1 // #1 - anonymous user
        }))[0]
      };

    }

  }

  async $auth({
    username,
    id,
    password
  }) {

    if ((!username || !id) && !password) {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 406;
      return {
        error: "No credentials"
      }
    }

    // no need to reauth if already authed
    if (this.ctx && !(await this.user.isAnonymous())) {
      const expirationDate = this.session.getExpirationTime();
      this.response.setHeader("Set-Cookie", `SESSID=${this.session.sessid}; Path=/; Expires=${new Date(expirationDate).toGMTString()}`);
      await this.session.prolong();
      return {
        user: await m.users.$me.$.call(this, {}), // fake context
        authed: true,
        session: {
          created: this.session.created,
          id: this.session.sessid,
          expire: expirationDate
        }
      }
    }

    const authed = await m.users.loginUser(id, username, password);
    if (authed) {
      // TODO: check that session is REALLY not exists
      const sessid = uuidv4().replace(/-/g, "");
      const session = new Session({
        sessid,
        id: authed.data.id
      });
      m.auth.sessions.set(sessid, session);
      session.save();
      const expirationDate = session.getExpirationTime();
      // let's set cookie on client
      if (this.ctx && this.isCausedByRequest())
        this.response.setHeader("Set-Cookie", `SESSID=${session.sessid}; Path=/; Expires=${new Date(expirationDate).toGMTString()}`);
      return {
        user: await m.users.$me.$.call({
          user: authed
        }, {}), // fake context
        authed: true,
        session: {
          created: session.created,
          id: session.sessid,
          expire: expirationDate
        }
      }
    } else {
      return {
        authed: false
      }
    }
  }

  async $deauth() {
    if (this.session === null) {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 401;
      return {
        error: "Not yet authorized"
      }
    }
    await this.session.delete();
    if (this.ctx) this.response.setHeader("Set-Cookie", `SESSID=; Path=/; Expires=Thu, 01 Jan 1970 00:00:00 GMT`);
    // on success deauth nothing to send, goodbye bae!
  }

  async $status({
    sessid
  }) {
    if (sessid && sessid !== "null" && typeof sessid === "string") {
      l.silly("Got force sessid on status call, let's try to auth...");
      const {
        session,
        user
      } = await m.auth.identify.call(this, this.request, this.response, sessid);
      const authed = !(await user.isAnonymous());
      if (!authed)
        l.silly("Failed to auth using force sessid!");
      return {
        authed,
        sessid
      }
    }
    return {
      "authed": !(await this.user.isAnonymous()),
      "sessid": (!this.session || !this.session.sessid ? null : this.session.sessid)
    }
  }



}

module.exports = Auth;
