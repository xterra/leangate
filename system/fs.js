'use strict';

// TODO: this variable should be more protected, I suppose?
let m;
let l;

class FS {

  constructor(modules) {
    m = modules;
    l = m.log.register(this);
  }

  $version = "1.0.0"

}

module.exports = FS;
