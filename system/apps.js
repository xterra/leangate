'use strict';

const fs = require('fs').promises;
const http = require('http');
const httpProxy = require('http-proxy');
const stream = require('stream');
const Docker = require('dockerode');

let m;
let l;

class App {

  data = {};

  constructor({
    id,
    name,
    icon,
    type,
    repo,
    owner
  }) {

    // TODO: checks
    this.data.id = id;
    this.data.name = name;
    this.data.icon = icon;
    this.data.type = type;
    this.data.repo = repo;
    this.data.owner = owner;
    this.data.upstream = null;
    this.data.uid = `${this.data.owner.data.id}.${this.data.id}`.toString();

    // TODO: destroy this object if container is dead

  }

  async run() {

    if (!(await this.isContainerExists())) {
      l.silly("Creating container...");
      await this.createContainer();
    }

    this.data.upstream = new httpProxy.createProxyServer({
      target: {
        host: this.data.uid,
        port: "10000"
      }
    });
    this.data.upstream.on('error', function(err, req, res) {
      l.error(`Upstream failure ${err} ${err.stack}`);
    });

    if (!(await this.isRunning()))
      return this.data.container.start();
  }

  async stop() {
    [this.data.container, this.data.containerDescription] = await this.findContainer();
    if (await this.isRunning())
      return this.data.container.stop();
  }

  async isRunning() {
    // TODO: rewrite this, this is extremely slow
    const [container, containerDescription] = await this.findContainer(); // if nothing found, do not overwrite previous results
    return containerDescription && containerDescription.State === "running";
  }

  async findContainer() {
    // TODO: rewrite this, this is extremely slow
    const currOwnerID = this.data.owner.data.id.toString();
    const currAppID = this.data.id.toString();
    return new Promise(function(resolve, reject) {
      m.apps.docker.listContainers({
        all: true
      }, function(err, containers) {
        if (err) return reject(err);
        for (const containerInfo of containers) {
          if (containerInfo.Labels.owner === currOwnerID &&
            containerInfo.Labels.application === currAppID) {
            return resolve([m.apps.docker.getContainer(containerInfo.Id), containerInfo]);
          }
        }
        resolve([null, null]);
      });
    });
  }

  async isContainerExists() {
    [this.data.container, this.data.containerDescription] = await this.findContainer();
    return !!this.data.container;
  }

  async destroyContainer() {
    await this.stop();
    if (await this.isContainerExists())
      return this.data.container.remove();
  }

  async pullImage() {
    const that = this;
    l.debug(`Pull image "${that.data.repo}"`);
    return new Promise(function(resolve, reject) {
      m.apps.docker.pull(that.data.repo.toString(), function(err, stream) {
        if (err) return reject(err);
        m.apps.docker.modem.followProgress(stream, (error, output) => {
          if (error)
            return reject(error);
          l.silly(`Image "${that.data.repo}" pulled`);
          resolve(output);
        } /*, (event) => console.log(event)*/ );
      });
    });
  }

  // async destroyNetwork() {
  //   l.silly("Searching for old network...");
  //   const networks = await m.apps.docker.listNetworks();
  //   for (const network of networks) {
  //     console.log(network);
  //     if (network.Network === this.data.uid) {
  //       l.silly(`Network ${network.Id} found, destroying...`);
  //       this.data.network = m.apps.docker.getNetwork(network.Id);
  //       this.data.network.remove();
  //       l.silly(`Network ${network.Id} destroyed!`);
  //     }
  //   }
  // }

  // async connectContainer() {
  //   l.silly("Creating new network...");
  //   const network = await m.apps.docker.createNetwork({
  //     name: this.data.uid.toString()
  //   });
  //   l.silly("Connecting containers to network...");
  //   this.data.network = network;
  //   await network.connect({
  //     Container: this.data.uid.toString(),
  //     Internal: true
  //   });
  //   // TODO: fix this bug
  //   // setTimeout(function(){
  //   //   network.connect({
  //   //     Container: "leangate"
  //   //   });
  //   // }, 1000);
  //   await network.connect({
  //     Container: "leangate"
  //   });
  //   l.silly("Containers connected to network!");
  // }

  async connectContainer() {
    l.silly("Connecting container to network...");
    await m.apps.network.connect({
      Container: this.data.uid.toString(),
    });
  }

  async createContainer() {
    await this.pullImage();
    // l.silly("Removing old network...");
    // await this.destroyNetwork();
    l.silly("Creating container...");
    const container = await m.apps.docker.createContainer({
      name: this.data.uid.toString(),
      Image: this.data.repo.toString(),
      AttachStdin: false,
      AttachStdout: true,
      AttachStderr: true,
      Tty: true,
      // TODO: override cmd
      //Cmd: ['/bin/bash', '-c', 'tail -f /var/log/dmesg'],
      OpenStdin: false,
      StdinOnce: false,
      Labels: {
        "owner": this.data.owner.data.id.toString(),
        "application": this.data.id.toString()
      },
      ExposedPorts: {
        "10000/tcp": {}
      },
      Mounts: [{
        "Target": "/data",
        "Source": "/usr/share/leangate/files",
        "Type": "bind",
        "ReadOnly": false
      }]
      //Links: ["leangate:leangate"]
    });
    this.data.container = container;
    l.silly("Connecting container to network...");
    await this.connectContainer();
    // TODO: write info about this container to user file
    // and restore container on system startup
    // or destroy on user deletion
    return container;
  }

}

class Apps {

  all = [];

  constructor(modules) {
    m = modules;
    l = m.log.register(this);

    this.docker = new Docker({
      socketPath: '/var/run/docker.sock'
    });

  }

  async init() {

    this.network = false;
    const networks = await m.apps.docker.listNetworks();
    for (const network of networks) {
      if (network.Name === "leangate") {
        l.silly(`Network ${network.Id} found, nothing to create!`);
        this.network = m.apps.docker.getNetwork(network.Id);
        // await this.network.remove();
        // l.silly(`Network ${network.Id} destroyed!`);
      }
    }

    if (!this.network) {
      l.silly(`Network not found, creating...`);
      this.network = await m.apps.docker.createNetwork({
        name: "leangate"
      });
      l.silly(`Network ${this.network.Id} created!`);
    }

    l.silly("Connecting container to network...");
    try {
      await this.network.connect({
        Container: "leangate",
        Internal: true
      });
    } catch (err) {}

    await this.listen();

  }

  async listen() {

    const proxyServer = http.createServer(function(request, response) {
      //console.log("web")
      //console.log("! request !", this);
      //console.log("! upstream !", m.apps.all[0]);
      m.apps.handle({
        type: "web",
        request,
        response
      });
    });
    proxyServer.on("upgrade", function(request, socket, head) {
      //console.log("! upgrade !", request, socket, head, m.apps.all[0].data.upstream);
      m.apps.handle({
        type: "ws",
        request,
        socket,
        head
      });
    });

    proxyServer.listen(8081);

  }

  async handle({
    type,
    request,
    response,
    socket,
    head
  }) {
    //if(response) this.response.setHeader('Access-Control-Allow-Origin', host ? `http://${this.request.host}:80,http://${host}:8081,ws://${host}:8081` : "*");
    const appIDnSessID = request.url.split("app=")[1];
    const [appID, sessID] = appIDnSessID.split(".");
    const {
      user
    } = await m.auth.identify(request, response, sessID);
    if (await user.isAnonymous()) {
      l.debug("Anonymous tried to access proxy!");
      return;
    }
    l.silly(`${type.toUpperCase()}-proxy -> ${user.data.id}.${appID}`);
    let app = null
    for (const thatApp of m.apps.all) {
      if (thatApp.data.owner.data.id === user.data.id && thatApp.data.id.toString() === appID.toString() && await thatApp.isRunning()) {
        app = thatApp;
        break;
      }
    }
    if (!app) {
      l.error(`App ${user.data.id}.${appID} is not running`);
      return;
    }
    if (type === "web") {
      try {
        app.data.upstream.web(request, response);
      } catch (err) {
        l.error(`HTTP failure ${err} ${err.stack}`);
      }
    } else {
      try {
        app.data.upstream.ws(request, socket, head);
      } catch (err) {
        l.error(`WS failure ${err} ${err.stack}`);
      }
    }
  }

  async $listInstalledApps() {

    // todo: read this from user profile, but for now show all in fs

    const apps = [];

    const files = await fs.readdir("/data/apps");

    for (const file of files) {
      try {
        let appMetadata = require(`/data/apps/${file}/manifest.json`);
        appMetadata["id"] = file;
        apps.push(appMetadata);
      } catch (err) {
        l.error(`Failed to read application manifest ${file}`);
      }
    }

    return apps;

  }

  async $listRunningApps() {

    // todo: show app resources usage

    const list = [];

    for (const app of m.apps.all) {
      if (app.data.owner.data.id === this.user.data.id && await app.isRunning()) {
        list.push({
          id: app.data.id,
          name: app.data.name,
          icon: app.data.icon,
        })
      }
    }

    return list;

  }

  async waitForAppStarted(container) {
    l.silly("Waiting for app ready...");
    return new Promise((resolve, reject) => {

      let responded = false;

      const interval = setInterval(function() {

        var logStream = new stream.PassThrough();
        logStream.on('data', function(chunk) {
          l.silly("Got XPRA log chunk");
          const executions = chunk.toString('utf8').split("killing xvfb with pid");
          if (/xpra is ready/.test(executions[executions.length - 1])) {
            l.silly("Seems XPRA is ready");
            responded = true;
            clearInterval(interval);
            return resolve();
          }
        });
        container.logs({
          follow: true,
          stdout: true,
          stderr: true
        }, function(err, stream) {
          if (err) {
            l.error(err.message);
            clearInterval(interval);
            return reject();
          }
          container.modem.demuxStream(stream, logStream, logStream);
          stream.on('end', function() {
            l.silly("Stream destroyed");
          });
          setTimeout(function() {
            stream.destroy();
          }, 450);
        });

      }, 500);

      setTimeout(function() {
        l.error("Timeout - app is not started yet");
        clearInterval(interval);
        reject();
      }, 15000);

    });
  }

  async $runApp({
    id,
    force
  }) {

    if (typeof id !== "string" && typeof id !== "number") {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 406;
      return {
        error: "Invalid application ID specified"
      }
    }

    const metrics = await m.system.$getClusterMetrics.call({
      user: {
        data: {
          id: 0
        }
      },
      session: {
        sessid: 0
      }
    });

    const uid = `${this.user.data.id}.${id}`;

    l.silly("Searching for already running container...");
    for (const node of metrics) {
      for (const container of node.metrics.dockerContainers) {
        if (container.name === uid && container.state === "running") {
          l.silly(`Found already running container on node "${node.node}"`);
          return {
            node: node.node
          }
        }
      }
    }

    if (!force) {

      l.silly("Searching for best node to start");

      let nodeName;
      let lessLoad = 101;
      for (const node of metrics) {
        if (node.metrics.cpuLoad.currentload < lessLoad) {
          lessLoad = node.metrics.cpuLoad.currentload;
          nodeName = node.node;
        }
      }

      l.silly(`Selected best node: "${nodeName}", trying to call...`);

      const {
        respond,
        data
      } = await m.system.perform.call(this, {
        node: nodeName,
        endpoint: "apps/runApp",
        parameters: {
          id,
          force: true
        }
      });
      return {
        node: nodeName
      }

    } else {

      l.silly(`Starting app ${id} for user ${this.user.data.id}`);

      // is app already running?
      for (const app of m.apps.all) {
        if (app.data.id === id && app.data.owner.data.id === this.user.data.id) {
          l.silly(`App ${id} object found in memory`);
          if (await app.isRunning()) {
            l.silly(`App ${id} already running`);
            const [container, containerDescription] = await app.findContainer(); // WA: load data about status
            return this.response.end(JSON.stringify({
              status: containerDescription.Status ? containerDescription.Status : false
            }));
          } else {
            l.silly(`App ${id} is not running, trying to handle situation...`);

            if (!(await app.isContainerExists())) {
              l.silly(`App ${id} is not running, and container is destroyed. Trying to recreate it...`);
              app.data.containerDescription = false;
              app.data.container = false;
            }

            l.silly(`App ${id} is not running, trying to start it again...`);
            try {
              await app.run();
              l.silly(`Container created again`);
              await m.apps.waitForAppStarted(app.data.container);
              // TODO: check: app started?
              this.response.end(JSON.stringify({
                status: false
              }));
              return;
            } catch (err) {
              l.error(`Failed to start app ${id} ${err} ${err.stack}`);
              this.respondCode = 500;
              return {
                "error": "Failed to start app"
              }
            }
          }

        }
      }

      l.silly(`App ${id} is not loaded in memory yet`);

      let appMetadata;
      try {
        appMetadata = require(`/data/apps/${id}/manifest.json`);
      } catch (err) {
        l.error(`Failed to app ${id} metadata ${err} ${err.stack}`);
        this.respondCode = 500;
        return {
          error: "Failed to load application"
        }
      }

      l.silly(`Starting app ${id}...`);

      const app = new App({
        id,
        name: appMetadata.name,
        icon: appMetadata.icon,
        type: appMetadata.type,
        repo: appMetadata.repo,
        owner: this.user
      });
      m.apps.all.push(app)

      try {
        await app.run();
        l.debug(`App ${id} started!`);
        await m.apps.waitForAppStarted(app.data.container);
        l.debug(`App ${id} ready!`);
        // TODO: check app started
        this.response.end(JSON.stringify({
          status: false
        }));
      } catch (err) {
        l.error(`Failed to start app ${id} ${err} ${err.stack}`);
        this.respondCode = 500;
        return {
          "error": "Failed to start app"
        }
      }
    }

  }

  async $stopApp({
    id,
    force
  }) {

    if (typeof id !== "string" && typeof id !== "number") {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 406;
      return {
        error: "Invalid application ID specified"
      }
    }

    if (!force) {

      const metrics = await m.system.$getClusterMetrics.call({
        user: {
          data: {
            id: 0
          }
        },
        session: {
          sessid: 0
        }
      });

      const uid = `${this.user.data.id}.${id}`;

      l.silly("Searching for already running container...");

      for (const node of metrics) {
        for (const container of node.metrics.dockerContainers) {
          if (container.name === uid && container.state === "running") {
            l.silly(`Found already running container on node "${node.node}, trying to stop..."`);

            const {
              respond,
              data
            } = await m.system.perform.call(this, {
              node: node.node,
              endpoint: "apps/stopApp",
              parameters: {
                id,
                force: true
              }
            });
            return {
              node: node.node
            }

          }
        }
      }

      return {
        "error": "App is not running"
      }

    } else {

      l.silly(`Force app ${id} stop called`);

      for (const app of m.apps.all) {
        if (app.data.id === id && app.data.owner.data.id === this.user.data.id && await app.isRunning()) {
          try {
            await app.stop();
            l.silly(`App ${id} stopped`);
            // TODO: check did app stopped
            return {};
          } catch (err) {
            l.error(`Failed to stop app ${app.data.id} ${err} ${err.stack}`);
            this.respondCode = 500;
            return {
              "error": "Failed to stop app"
            }
          }
        }
      }

      l.silly(`App ${id} is not found to be stopped`);

      return {
        "error": "App is not running"
      }

    }

  }

  async $pauseApp({
    id
  }) {

    if (typeof id !== "string" && typeof id !== "number") {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 406;
      return {
        error: "Invalid application ID specified"
      }
    }

  }

  async $resumeApp({
    id
  }) {

    if (typeof id !== "string" && typeof id !== "number") {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 406;
      return {
        error: "Invalid application ID specified"
      }
    }

  }

  async $migrateApp({
    node,
    id
  }) {

    l.silly(`Migration called to node ${node}, searching for app ${this.user.data.id}.${id}`);

    let foundApp = null;
    for (const app of m.apps.all) {
      if (app.data.id === id && app.data.owner.data.id === this.user.data.id && await app.isRunning()) {
        l.silly(`App ${id} found in memory and it's running`);
        foundApp = app;
      }
    }

    if (!foundApp) {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 406;
      return {
        error: "App is not running"
      }
    }

    l.silly(`Stopping app ${foundApp.data.id}...`);
    foundApp.stop().then(_ => {
      l.silly(`App ${foundApp.data.id} stopped!`);
    }).catch(err => {
      l.silly(`Failed to stop app ${foundApp.data.id}! ${err} ${err.stack}`);
    });

    l.silly(`Starting app ${foundApp.data.id} on another node ${node}...`);

    try {
      const {
        respond,
        data
      } = await m.system.perform.call(this, {
        node,
        endpoint: "apps/runApp",
        parameters: {
          id
        }
      });
      return {
        node,
        sessid: respond.headers['x-sessid']
      }
    } catch (err) {
      if (this.ctx && this.isCausedByRequest()) this.respondCode = 500;
      l.error(`Migration failure ${err} ${err.stack}`);
      return {
        error: "Failed to migrate"
      }
    }

  }



}

module.exports = Apps;
