#!/usr/bin/env bash

set -e

cd $(dirname "$0")

echo "STARTING" > STATE

# TODO: do not start if already running

echo "Starting service..."

# try to install dependencies, but do not fail if no internet
npm install --prefix watchdog || true
npm install --prefix system || true

# cleanup system
docker system prune -f

# stop previous watchdog
pkill -f "node watchdog" || true

# start watchdog
node watchdog
