FROM node:12-alpine

ENV USER=leangate
ENV UID=2001
ENV GID=2000

RUN apk update \
 && apk upgrade \
 && apk add bash \
 && apk add wget \
 && apk add git

RUN wget -O /tmp/leangate-horizon-prototype.tar.bz2 https://gitlab.com/xterra/leangate-horizon/-/archive/prototype/leangate-horizon-prototype.tar.bz2 \
 && tar xvjf /tmp/leangate-horizon-prototype.tar.bz2 -C /tmp \
 && mv /tmp/leangate-horizon-prototype /horizon \
 && chmod -R 755 /horizon

RUN addgroup --gid "$GID" "$USER" \
 && adduser \
     --disabled-password \
     --gecos "" \
     --home "/leangate" \
     --ingroup "$USER" \
     --no-create-home \
     --uid "$UID" \
     "$USER"

#USER leangate

ENTRYPOINT ["node", "/leangate"]
