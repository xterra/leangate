'use strict';
const fs = require('fs');
const http = require('http');
const querystring = require('querystring');
const uuidv4 = require('uuid/v4');
const {Docker} = require("node-docker-api");
const tar = require('tar-fs');
const { execSync } = require('child_process');
const si = require('systeminformation');

const readFile = async (fileName) => {
  return new Promise((resolve, reject) => {
    fs.readFile(fileName, (err, data) => {
        if (err) return reject(err);
        resolve(data);
    });
  });
}

const writeFile = async (fileName, data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(fileName, data, (err) => {
        if (err) return reject(err);
        resolve();
    });
  });
}

class Watchdog {

  constructor(options = {}) {

    if (typeof options.skipConfigsFile !== "boolean"
        || !options.skipConfigsFile) {
      try {
        options = JSON.parse(fs.readFileSync("watchdog-configs.json"));
      } catch (err) {
        console.warn("Not possible to open config file, skipping...");
      }
    }

    if (typeof options.watchdogToken === "string"
        && options.watchdogToken.length > 8)
      this.token = options.watchdogToken;

    if (typeof options.dockerSocket === "string")
      this.docker.socketPath = options.dockerSocket;

    if (typeof options.dockerfileLocation === "string")
      this.system.dockerfileLocation = options.dockerfileLocation;

    if (typeof options.synchronizeInterval === "number"
        && options.synchronizeInterval > 10)
      this.synchronize.interval = options.synchronizeInterval;

    if (typeof options.noAutoBoot === "undefined" || ! options.noAutoBoot)
      this.boot().then(() => {
        console.info("Boot done");
      }).catch(error => {
        console.error("A problem occurred on boot. See", error);
        process.exit(1);
      });

  }

  synchronize = {
    interval : 1000,
    tick : 0,
    heart : null,
    lastHealthy : false,
    schedule : () => {
      console.log("Scheduling Sync");
      this.synchronize.unschedule();
      this.synchronize.heart = setInterval(() => {
        this.synchronize.perform().then(() => {
          this.state.update("HEALTHY");
          if(!this.synchronize.lastHealthy) {
            this.synchronize.lastHealthy = true;
            console.log("Healthy");
          }
        }).catch((err) => {
          this.state.update("BROKEN");
          if (this.synchronize.lastHealthy) {
            this.synchronize.lastHealthy = false;
            console.log("Unhealthy", err);
          }
        });
      }, this.synchronize.interval);
      console.info("Sync");
    },
    unschedule : () => {
      if (this.synchronize.heart !== null) {
        clearInterval(this.synchronize.heart);
        console.log("Unscheduled old Sync");
      }
    },
    perform : async () => {
      // todo
      //if(this.synchronize.tick++ > 2) {
        this.synchronize.tick = 0;
        console.log("Gathering host facts...");
        try {
          const osInfo = await si.osInfo();
          const dockerInfo = await si.dockerInfo();
          const dockerContainers = await si.dockerContainers(true);
          const cpuInfo = await si.cpu();
          const cpuLoad = await si.currentLoad();
          const ram = await si.mem();
          const time = si.time();
          console.log(`System uptime: ${time.uptime}`);
          console.log(`CPU load: ${Math.floor(cpuLoad.currentload)}%`);
          console.log(`RAM load: ${Math.floor(ram.free/ram.total*100)}%`);
          // let's try to send this information to system running at 172.17.0.2
          this.synchronize.sendData({
            osInfo,
            dockerInfo,
            dockerContainers,
            cpuInfo,
            cpuLoad,
            ram,
            time
          });
        } catch (err) {
          console.error("Failed to gather facts");
        }
      //}
      return await this.system.isContainerRunning();
    },
    sendData : async (data) => {

      const json = JSON.stringify(data);

      const request = http.request({
        host: "172.17.0.2", // TODO: correct this digging real container address
        port: '8082',
        path: `/`,
        method: 'POST',
        headers: {
          'X-Watchdog-Token': this.token,
          'Content-Type': 'application/json',
          'Content-Length': Buffer.byteLength(json)
        }
      }, function(res) {
        // TODO?
      }).on("error", (err) => {
        // TODO?
        console.error(err);
      });

      request.write(json);
      request.end();

    }
  }

  docker = {
    connection : null,
    socketPath : "/var/run/docker.sock",
    connect : async () => {
      console.log("Trying to connect to Docker Daemon...");
      try {
        this.docker.connection = new Docker({ socketPath:
          this.docker.socketPath });
      } catch (err) {
        console.error("Can't connect to Docker Daemon. Check it's status and \
try again.");
        throw err;
      }
      if (! await this.docker.isConnectionAlive()) {
        throw new Error("Connection is not alive");
      }
      console.info("Successfully connected to Docker Daemon");
    },
    isConnectionAlive : async () => {

      if (typeof this.docker.connection !== "object"
          || ! this.docker.connection
          || typeof this.docker.connection.container !== "object"
          || typeof this.docker.connection.container.list !== "function")
        return false;

      try {
        // TODO: use another command,
        // because on 1000 containers this operation will const a lot
        await this.docker.connection.container.list();
      } catch (err) {
        return false;
      }

      return true;

    }
  };

  state = {
    last : null,
    changed : new Date(),
    update : async (state) => {
      if (this.state.last !== state) {
        this.state.last = state;
        this.state.changed = new Date();
        await writeFile("STATE", state);
      }
    },
  }

  system = {
      dockerfileLocation : "/etc/leangate",
      container : null,
      isImageExists : async () => {
        try {
          console.log("Is image exists? Status:", await this.docker.connection.image.get('leangate').status());
          return true;
        } catch (err) {
          return false;
        }
      },
      isContainerExists : async () => {
        try {
          if (!this.system.container) {
            await this.system.findContainer();
          }
          return this.system.container !== null;
        } catch (err) {
          return false;
        }
      },
      isContainerRunning : async () => {
        try {
          if (!this.system.container) {
            await this.system.findContainer();
          }
          const status = await this.system.container.status();
          return status.data.State.Running;
        } catch (err) {
          return false;
        }
      },
      buildImage : async () => {
        const tarStream = tar.pack(this.system.dockerfileLocation, {
          entries: ["Dockerfile"]
        });
        console.log(`Trying to build image from location \
${this.system.dockerfileLocation}`, tarStream);
        const stream = await this.docker.connection.image.build(tarStream, {
          t: 'leangate'
        });
        return new Promise((resolve, reject) => {
            stream.on('data', data => console.log(data.toString()));
            stream.on('end', resolve);
            stream.on('error', reject);
          });
      },
      findContainer : async () => {
        const list = await this.docker.connection.container.list();
        console.log("Find container, list:", list);
        for (let i = 0; i < list.length; i++) {
          if (list[i].data.Image === "leangate") {
            this.system.container = list[i];
          }
        }
      },
      destroyContainer : async () => {
        await this.system.container.delete({ force: true })
        this.system.container = null;
      },
      createContainer : async () => {
        console.log("Creating container...");
        this.system.container = await this.docker.connection.container.create({
          Image: 'leangate',
          ExposedPorts: {
            "8080/tcp": {},
            "8081/tcp": {},
            "8082/tcp": {},
            "8443/tcp": {}
          },
          HostConfig: {
            PortBindings: {
              "8080/tcp": [{ "HostPort": "80" }],
              "8081/tcp": [{ "HostPort": "8081" }],
              "8443/tcp": [{ "HostPort": "443" }]
            },
            Mounts: [
                {
                   "Target":   "/leangate",
                   "Source":   "/etc/leangate/system",
                   "Type":     "bind",
                   "ReadOnly": true
                },
                {
                   "Target":   "/configs",
                   "Source":   "/etc/leangate/configs",
                   "Type":     "bind",
                   "ReadOnly": true
                },
                {
                   "Target":   "/data",
                   "Source":   "/usr/share/leangate",
                   "Type":     "bind",
                   "ReadOnly": false
                },
                {
                   "Target":   "/var/run/docker.sock",
                   "Source":   this.docker.socketPath,
                   "Type":     "bind"
                }
             ]
          },
          RestartPolicy: {
            Name : "on-failures",
            MaximumRetryCount: 16
          },
          name: 'leangate'
        });
        console.log("Created. Status:", await this.system.container.status());
      },
      startContainer : async () => {
        console.log("Trying to start...");
        await this.system.container.start();
        console.log("Created. Status:", await this.system.container.status());

      },
      stopContainer : async () => {
        await this.system.container.stop();
      },
      up : async () => {

        // in case, when somebody trying to restart container
        // in case, when system shutdowned suddenly
        await this.system.down();

        // in case, when system just installed or upgraded
        console.log("Searching for Docker image");
        if(! await this.system.isImageExists()) {
          console.warn("Image is not exist");
          await this.system.buildImage();
        }

        console.log("Starting Docker container");
        await this.system.createContainer();
        await this.system.startContainer();

      },
      down : async () => {

        console.log("Searching for running container...");
        if (await this.system.isContainerRunning()) {
          console.log("Found.");
          console.log("Trying to stop old container...");
          await this.system.stopContainer();
          console.log("Stopped.");
        } else {
          console.log("Nothing found.");
        }

        console.log("Searching for old container...");
        if (await this.system.isContainerExists()) {
          console.log("Found.");
          console.log("Trying to destroy old container...");
          await this.system.destroyContainer();
          console.log("Destroyed.");
        } else {
          console.log("Nothing found.");
        }

      }
    }

  async boot () {

    console.log("Start boot");

    this.state.update("STARTING");

    // try to connect to docker
    if (! await this.docker.isConnectionAlive()) {
      console.log("Docker connection is not established yet");
      await this.docker.connect();
    } else {
      console.warn("Docker connection already established");
    }

    if (typeof this.token !== "string") {

      // try to load token from file
      try {

        this.token = await readFile("system/watchdogToken");
        console.info("Watchdog token loaded");

      } catch (err) {

        // possibly file do not exists, let's create it
        console.warn("Can't load watchdog token, trying to regenerate it...");
        this.token = uuidv4();
        await writeFile("system/watchdogToken", this.token);
        console.info("Watchdog token created");

      }

    }

    console.log("Bringing system up...");
    await this.system.up();

    this.state.update("UP");

    console.log("System up!");

    this.synchronize.schedule();

  }

}

if (require.main === module) {

  const watchdog = new Watchdog();

  let shutdownInProgress = false;

  process.stdin.resume();

  function exitHandler(options, exitCode) {
    if (!shutdownInProgress) {
      console.log(`Initiating system shutdown (code ${exitCode})...`);
      shutdownInProgress = true;
      if (exitCode === 0) {
        watchdog.state.update("DOWN");
      } else {
        watchdog.state.update("FAILED");
      }
      watchdog.synchronize.unschedule();
      watchdog.system.down().then(_=>{
        process.exit(exitCode);
      }).catch(err => {
        console.error("!!! SYSTEM FAILED TO SHUTDOWN GRACEFULLY !!!");
        console.error(err);
        process.exit(1);
      });
    } else {
      console.log("System already trying to shutdown!");
    }
  }

  //do something when app is closing
  process.on('exit', exitHandler.bind(null, { reason: "end" }));

  //catches ctrl+c event
  process.on('SIGINT', exitHandler.bind(null, { reason: "crtlc" }));

  // catches "kill pid" (for example: nodemon restart)
  process.on('SIGUSR1', exitHandler.bind(null, { reason : "kill", type : 1 }));
  process.on('SIGUSR2', exitHandler.bind(null, { reason : "kill", type : 2 }));
  process.on('SIGTERM', exitHandler.bind(null, { reason : "kill", type : 3 }));
  process.on('SIGHUP', exitHandler.bind(null, { reason : "shutdown" }));

  //catches uncaught exceptions
  process.on('uncaughtException', exitHandler.bind(null, { reason: "exception" }));

} else {
  module.exports = Watchdog;
}
